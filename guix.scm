(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix git-download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages crypto)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(define-public guile-sodium
  (package
    (name "guile-sodium")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/guile-sodium")
               (commit "0344d63de6035f8e666085f88a49ffdcbaf0e7e5")))
        (file-name (git-file-name name version))
        (sha256 (base32 "00pr4j1biiklcr4q3x38fi45md866bql57aq2aima826jfkwws05"))))
    (build-system gnu-build-system)
    (arguments `())
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs `(("libsodium" ,libsodium)))
    (synopsis "Guile bindings to libsodium.)")
    (description
      "This package provides bindings to libsodium which provides core cryptogrpahic primitives needed to build higher-level tools.")
    (home-page
      "https://gitlab.com/openengiadina/guile-sodium")
    (license license:gpl3+)))

(define-public guile-schemantic
  (package
    (name "guile-schemantic")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/guile-schemantic")
               (commit "9f32f25bd7e67b34b0f0efb39d984a5c46fc294a")))
        (file-name (git-file-name name version))
        (sha256 (base32 "01raz994vj9qvl91ipxpgrs4szc5wy1bxxxnhrrg6mpwia8954xm"))))
    (build-system gnu-build-system)
    (arguments `())
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs `(("guile-rdf" ,guile-rdf)))
    (synopsis "Guile library for the Semantic Web")
    (description
      "Guile Schemantic is a Guile library for the Semantic Web and implements the Resource Description Framework (RDF).")
    (home-page
      "https://gitlab.com/openengiadina/guile-schemantic")
    (license license:gpl3+)))

(package
 (name "guile-content-addressable-rdf")
 (version "0.1")
 (source
  "./guile-content-addressable-rdf-0.1.tar.gz")
 (build-system gnu-build-system)
 (arguments `())
 (native-inputs
  `(("autoconf" ,autoconf)
    ("automake" ,automake)
    ("pkg-config" ,pkg-config)
    ("texinfo" ,texinfo)))
 (inputs `(("guile" ,guile-3.0)))
 (propagated-inputs
  `(("guile-sodium" ,guile-sodium)
    ("guile-schemantic" ,guile-schemantic)))
 (synopsis
  "A proposal (and Guile implementation) of how to make RDF content-addressable.")
 (description "")
 (home-page
  "https://gitlab.com/openengiadina/content-addressable-rdf")
 (license license:gpl3+))
