(define-module (rdf fragment-graph)
  #:use-module (rdf rdf)
  #:use-module (web uri)
  #:use-module (csexp)
  #:use-module (radix-sort)
  #:use-module (sodium generichash)
  #:use-module (base32)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (vhash index)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:use-module (ice-9 exceptions)
  #:export (make-rdf-fragment-graph
            rdf-fragment-graph?
            rdf-fragment-graph-base-subject
            rdf-fragment-graph-set-base-subject!

            rdf-fragment-graph-add-statement
            rdf-fragment-graph-add-fragment-statement
            rdf-fragment-graph-add-triple
            rdf-fragment-graph-add-triples

            rdf-fragment-graph->rdf-graph
            rdf-fragment-graph->csexp

            rdf-fragment-graph-make-content-addressable!))


;; Fragment Graph

(define-record-type <rdf-fragment-graph>
  (make-rdf-fragment-graph* base-subject
                            statements
                            fragment-statements)
  rdf-fragment-graph?
  (base-subject rdf-fragment-graph-base-subject set-base-subject!)
  (statements get-statements)
  (fragment-statements get-fragment-statements))

(define (rdf-fragment-graph-set-base-subject! fg base-subject)
  (set-base-subject! fg base-subject)
  fg)

(define (make-rdf-fragment-graph base-subject)
  "Returns an empty Fragment Graph for given base subject."
  (make-rdf-fragment-graph*
   (string->uri base-subject)
   vlist-null
   vlist-null))

(define (is-fragment? base-subject uri)
  (let*
      ((uri (if (uri? uri) uri (string->uri uri)))
       (uri-without-fragment
        (build-uri (uri-scheme uri)
                   #:userinfo (uri-userinfo uri)
                   #:host (uri-host uri)
                   #:port (uri-port uri)
                   #:path (uri-path uri)
                   #:query (uri-query uri))))

    (equal? uri-without-fragment
            base-subject)))


(define (rdf-fragment-graph-add-statement fg predicate object)
  (make-rdf-fragment-graph*
   (rdf-fragment-graph-base-subject fg)
   (index-add (get-statements fg)
              (list predicate object))
   (get-fragment-statements fg)))

(define (rdf-fragment-graph-add-fragment-statement fg fragment-identifier predicate object)
  (make-rdf-fragment-graph*
   (rdf-fragment-graph-base-subject fg)
   (get-statements fg)
   (index-add
    (get-fragment-statements fg)
    (list fragment-identifier predicate object))))

(define (rdf-fragment-graph-add-triple triple fg)
  "Return a Fragment Graph with the triple added as statements or fragment statements."
  (let*
      ((subject (rdf-triple-subject triple))
       (subject-uri (string->uri subject)))

    (if (uri? subject-uri)

        (cond

         ;; if subject of triple to add is the base subject
         ((equal? subject-uri (rdf-fragment-graph-base-subject fg))

          ;; then add the triple as statement
          (rdf-fragment-graph-add-statement fg
                         (rdf-triple-predicate triple)
                         (rdf-triple-object triple)))

         ;; if subject in triple is a fragment of base subject
         ((is-fragment? (rdf-fragment-graph-base-subject fg) subject-uri)

          ;; then add the triple as a fragment statement
          (rdf-fragment-graph-add-fragment-statement fg
                                  (uri-fragment subject-uri)
                                  (rdf-triple-predicate triple)
                                  (rdf-triple-object triple)))

         (else fg))


        ;; return the fragment graph without adding triple
        fg)))

(define (rdf-fragment-graph-add-triples triples fg)
  "Returns a Fragment Graph with triples added as statements or fragment statements."
  (fold rdf-fragment-graph-add-triple fg triples))

(define (rdf-fragment-graph->rdf-graph fg)
  (append

   ;; statements to triples
   (map (match-lambda ((predicate object)
                       (make-rdf-triple
                        (uri->string (rdf-fragment-graph-base-subject fg))
                        predicate
                        object)))
        (index->list (get-statements fg)))

   ;; fragment statements to triples
   (map (match-lambda
          ((fragment-identifier predicate object)
           (make-rdf-triple
            (uri->string
             (let ((uri (rdf-fragment-graph-base-subject fg)))
               (build-uri (uri-scheme uri)
                          #:userinfo (uri-userinfo uri)
                          #:host (uri-host uri)
                          #:port (uri-port uri)
                          #:path (uri-path uri)
                          #:query (uri-query uri)
                          #:fragment fragment-identifier)))
            predicate
            object)))
        (index->list (get-fragment-statements fg)))))


(define fragment-reference?
  (match-lambda ((f fragment-id) #t)
                (_ #f)))

(define (predicate->csexp p)
  (cond
   ((string? p) p)

   ((fragment-reference? p) p)

   (else (raise-exception
          (make-exception-with-message "predicate is not an IRI or fragment reference")))))

(define (object->csexp o)
  (cond
   ;; object is an iri
   ((string? o) o)

   ((rdf-literal? o)
    (if (equal? (rdf-literal-type o)
                "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString")
        `(l ,(rdf-literal-lexical-form o)
            ,(rdf-literal-type o)
            ,(rdf-literal-langtag o))
        `(l ,(rdf-literal-lexical-form o)
            ,(rdf-literal-type o))))

   ((fragment-reference? o) o)

   (else
    (raise-exception
     (make-exception-with-message
      "object is not an iri, fragment reference or literal")))))

(define (rdf-fragment-graph->csexp fg)
  (let* ((statements
          (map (match-lambda
                 ((predicate object)
                  `(s ,(predicate->csexp predicate)
                      ,(object->csexp object))))
               (index->list (get-statements fg))))

         (fragment-statements
          (map (match-lambda
                 ((fragment-identifier predicate object)
                  `(fs ,fragment-identifier
                      ,(predicate->csexp predicate)
                      ,(object->csexp object))))
               (index->list (get-fragment-statements fg))))

         ;; NOTE: We transform sexp to bytevector (csexp), sort and then parse back to sexp. This is not very efficient and can be improved.
         (sorted-statements
          (map bytevector->csexp
               (radix-sort
                (map csexp->bytevector
                     (append statements fragment-statements))))))

    (csexp->bytevector
     (cons 'rdf
           sorted-statements))))


(define (blake2b bv)
  (string->uri
   (string-append "urn:blake2b:"
                  (base32-encode (crypto-generichash bv)))))

(define* (rdf-fragment-graph-make-content-addressable! fg #:key (hash-fn blake2b))
  (rdf-fragment-graph-set-base-subject! fg (hash-fn (rdf-fragment-graph->csexp fg))))
