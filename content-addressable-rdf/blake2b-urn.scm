(define-module (content-addressable-rdf blake2b-urn)
  #:use-module (schemantic rdf)
  #:use-module (sodium generichash)
  #:use-module (content-addressable-rdf base32)
  #:export (blake2b-uri))

(define (blake2b-uri bv)
  (make-iri (string-append "urn:blake2b:"
                           (base32-encode (crypto-generichash bv)))))
