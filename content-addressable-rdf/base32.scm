(define-module (content-addressable-rdf base32)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 vlist)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-60)
  #:use-module (srfi srfi-158)
  #:export (base32-encode
            base32-decode

            base32-alphabet-rfc4648))

;; Base32 encoding of bytevectors
;;
;; TODO: padding

(define base32-alphabet-rfc4648
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567")

(define (make-encode-alphabet str)
  (alist->vhash
   (map
    cons
    (iota 32)
    (string->list str))))

(define (make-decode-alphabet str)
  (alist->vhash
   (map
    cons
    (string->list str)
    (iota 32))))

(define (quintet->alphabet-character q alphabet)
  (cdr
   (vhash-assv q alphabet)))

(define (alphabet-character->quintet char alphabet)
  (cdr (vhash-assv char alphabet)))

(define (pad-right lst n padding)
  (append
   lst
   (make-list (max 0 (- n (length lst))) padding)))

(define (make-quintet-generator bv)
  (gmap
   (compose list->integer (cut pad-right <> 5 #f))
   (ggroup
    (gbind
     (compose list->generator (cut integer->list <> 8))
     (bytevector->generator bv)) 5)))

(define* (base32-encode bv #:key (alphabet base32-alphabet-rfc4648))
  (let ((alphabet* (make-encode-alphabet alphabet)))
      (gaccumulate
       (string-accumulator)
       (gmap
        (cut quintet->alphabet-character <> alphabet*)
        (make-quintet-generator bv)))))

(define* (base32-decode str #:key (alphabet base32-alphabet-rfc4648))
  (let ((alphabet* (make-decode-alphabet base32-alphabet-rfc4648)))
    (gaccumulate
     (bytevector-accumulator)
     (gmap
      list->integer
      (gfilter
       (lambda (lst) (eqv? 8 (length lst)))
       (ggroup
        (gbind
         (compose list->generator
                  (cut integer->list <> 5)
                  (cut alphabet-character->quintet <> alphabet*))
         (string->generator str)) 8))))))
