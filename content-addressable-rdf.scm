(use-modules (rdf rdf)
             (turtle tordf)
             (turtle fromrdf)
             (web uri)
             (rnrs bytevectors)
             (rdf fragment-graph))

(string->uri "urn:blake2b:asdf")

(define clowns
  (turtle->rdf "./examples/clowns.ttl" "http://base.com/"))

(rdf-graph? clowns)

(rdf-fragment-graph-add-triples
 clowns
 (make-rdf-fragment-graph "https://test.example/notes/1"))

(define lingvoj
  (turtle->rdf "./examples/ontology_v2.33.ttl" "http://blups.com"))

(rdf-graph? lingvoj)

(car lingvoj)

(rdf-fragment-graph->csexp
 (rdf-fragment-graph-add-triples
  lingvoj
  (make-rdf-fragment-graph "http://www.lingvoj.org/ontology")))
